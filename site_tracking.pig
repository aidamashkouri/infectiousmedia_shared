REGISTER s3n://infectiousml/jars/elephant-bird-core-4.1.jar
REGISTER s3n://infectiousml/jars/elephant-bird-hadoop-compat-4.1.jar
REGISTER s3n://infectiousml/jars/elephant-bird-pig-4.1.jar
REGISTER s3n://infectiousml/jars/json-simple-1.1.jar
REGISTER s3n://infectiousml/jars/piggybank.jar;
REGISTER /home/hadoop/pig/udf/attr.py using streaming_python AS cumsum;

DEFINE CustomFormatToISO org.apache.pig.piggybank.evaluation.datetime.convert.CustomFormatToISO();
DEFINE ISOToUnix org.apache.pig.piggybank.evaluation.datetime.convert.ISOToUnix();

-------------------------------------------------------------------
-- load attribution data
-------------------------------------------------------------------

sky = LOAD 's3n://infectiousdata/skyscanner/raw/attribution/ym=201310/d=25/*' USING PigStorage ('\t')  AS
        (timestamp:int,
        user_id:chararray,
        country_code:chararray,
        associated_id:chararray,
        partner_name:chararray,
        program_name:chararray,
        type:chararray,
        sub_type:chararray);

sky_1 = ORDER sky by user_id, timestamp DESC;
sky_2 = FOREACH sky_1 GENERATE timestamp,user_id AS ss_uid, country_code AS country, CONCAT(associated_id,program_name) AS channel, 2 AS event_type;
--sky_3 = GROUP sky_2 BY (user_id);
--sky_4 = FOREACH sky_3 GENERATE group, cumsum.rollupsum(sky_2) AS record;
--cumsum.rollupsum(sky_3) AS record: {(timestamp:int,user_id:chararray, country_code:chararray, channel:chararray, event_type:int, channel_cumsum:int,recency:int};

-------------------------------------------------------------------
--load idb pixel data
-------------------------------------------------------------------

idb = LOAD 's3n://infectiousdata/idb/raw/pixel/ym=201310/d=25' USING
    com.twitter.elephantbird.pig.load.JsonLoader() AS ( json: map[] );
    idb = LOAD 's3n://infectiousdata/idb/raw/pixel/ym=201310/d=25' USING
    com.twitter.elephantbird.pig.load.JsonLoader() AS ( json: map[] );
idb_1 = FOREACH idb GENERATE
    (long)json#'timestamp' AS timestamp,
    (chararray)json#'ud1' AS ss_uid,
    (chararray)json#'apn_user_id' AS apn_uid,
    (int)json#'pixel_id' AS pixel_id;

 idb_2 = distinct idb_1;   

idb_2 = FILTER idb_2 BY (pixel_id == 252  and apn_uid != '0');

----------------------------------------------------------------------
-- leftjoin the idb.pixel to skyscanncer data on ss_uid
----------------------------------------------------------------------
joined_sky_idb = JOIN sky_2 by ss_uid LEFT OUTER, idb_2  BY ss_uid;
site_tracking = FOREACH joined_sky_idb GENERATE
  sky_2::ss_uid AS ss_uid,
  sky_2::timestamp AS timestamp,
  sky_2::channel AS channel,
  idb_2::apn_uid AS apn_uid,
  sky_2::country AS country,
  sky_2::event_type AS event_type;

