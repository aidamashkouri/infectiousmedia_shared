from pig_util import outputSchema
channels = ['IM Impression',
            'Conversion',
            'Kronenbourg',
            'AFF_NTW_00014_00001CJ (UK)',
            'AFF_DIR_00052_00009Opera (ES)',
            'SEM_GGL_00048_00001Marin (UK)',
            'SEM_GGL_00048_00004Marin (FR)',
            'DIS_CRT_00053_00001Criteo (UK)',
            'DIS_CRT_00053_00003Criteo (FR)',
            'AFF_PPC_00107_00001Ligatus (FR)',
            'AFF_WIG_00122_00001newsweb (FR)',
            'SEM_GGF_00065_00013Kenshoo (FR)',
            'SEM_GGF_00065_00039Kenshoo (UK)',
            'AFF_DIR_00023_00001Viamundis (IT)',
            'AFF_PPC_00004_00001TravelZoo (UK)',
            'AFF_PPC_00042_00001Mon Nuage (FR)',
            'SEM_YAH_00048_00004Marin (FR YAH)',
            'AFF_DIR_00104_00001Spain.info (ES)',
            'AFF_PPC_00026_00001Lastminute (UK)',
            'AFF_PPC_00026_00006Lastminute (FR)',
            'AFF_PPC_00026_00007Lastminute (SG)',
            'AFF_PPC_00010_00001DealChecker (UK)',
            'AFF_PPC_00018_00001Farecompare (UK)',
            'AFF_PPC_00018_00003Farecompare (ES)',
            'AFF_PPC_00018_00004Farecompare (DE)',
            'AFF_PPC_00018_00005Farecompare (AU)',
            'AFF_PPC_00018_00006Farecompare (NZ)',
            'AFF_PPC_00018_00014Farecompare (FR)',
            'AFF_PPC_00018_00020Farecompare (TH)',
            'AFF_PPC_00018_00022Farecompare (KR)',
            'AFF_DIR_00101_00002Which Budget (UK)',
            'AFF_NTW_00074_00002Tradedoubler (FR)',
            'AFF_NTW_00074_00004Tradedoubler (IT)',
            'AFF_NTW_00074_00005Tradedoubler (NL)',
            'AFF_NTW_00074_00009Tradedoubler (DK)',
            'AFF_NTW_00074_00011Tradedoubler (RU)',
            'AFF_PPC_00003_00001BookingBuddy (UK)',
            'AFF_PPC_00116_00001Intent-Media (US)',
            'API_DIR_00101_00122Which Budget (UK)',
            'AFF_NTW_00014_00002CJ Global (Global)',
            'DIS_CRH_00053_00001Criteo (UK) Hotels',
            'DIS_LGD_00120_00003Lagardere Boursier',
            'AFF_PPC_00006_00001Booking Wizard (UK)',
            'DIS_LGD_00120_00001Lagardere Sports_fr',
            'AFF_PPC_00008_00001airfaresflights (UK)',
            'DIS_IFX_00103_00006InfectiousMedia (FR)',
                       'AFF_PPC_00026_00010Lastminute - Hotels (UK)',
            'DIR_ECT_00000_00000Direct Type-ins (Global)',
            'AFF_PPC_00010_00003DealChecker - Hotels (UK)',
            'WEB_LNK_00000_00000Organic Referrals (Global)',
            'DIS_IFX_00103_00005Infectious Media (UK) static',
            'SEO_BRY_00000_00000SEO - Brand Queries (Global)',
            'CRM_ALT_00078_00001Skyscanner - Price Alerts (UK)',
            'CRM_ALT_00078_00009Skyscanner - Price Alerts (FR)',
            'SEO_BRN_00000_00000SEO - Non-Brand Queries (Global)',
            'CRM_ALT_00078_00000Skyscanner - Price Alerts (Global)']



def sumit(i, cumsum):
    feature = i[3]
    #feature = i[4]
    cumsum[feature] += 1
    hoo = cumsum[feature]
    return hoo, cumsum


@outputSchema("record: {(timestamp:int, user_id:chararray, country_code:chararray, channel:chararray, event_type:int, channel_cumsum:int,recency:int)}")
#@outputSchema("record: {(timestamp:int, ss_uid:chararray, apn_uid:chararray, country_code:chararray, channel:chararray, event_type:int, channel_cumsum:int,recency:int)}")
def rollupsum(group):
  group.sort(key=lambda x: x[0], reverse=False)
  cumsum = {}
  for c in channels:
    cumsum[c] = 0

  boo = []
  for j,i in enumerate(group):
    v, cumsum = sumit(i, cumsum)

    if j == 0:
        dt = 0
    else:
        dt = int(i[0]) - int(group[j-1][0])
    d = list(i)
    d.append(v)
    d.append(dt)

    boo.append(d)

  t = tuple(boo)
  #raise Exception(t)                                                                                                                                                                
  return t
