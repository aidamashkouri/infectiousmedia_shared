
REGISTER s3n://infectiousml/jars/elephant-bird-core-4.1.jar
REGISTER s3n://infectiousml/jars/elephant-bird-hadoop-compat-4.1.jar
REGISTER s3n://infectiousml/jars/elephant-bird-pig-4.1.jar
REGISTER s3n://infectiousml/jars/json-simple-1.1.jar
REGISTER s3n://infectiousml/jars/piggybank.jar;
REGISTER /home/hadoop/pig/udf/attr.py using streaming_python AS cumsum;

DEFINE CustomFormatToISO org.apache.pig.piggybank.evaluation.datetime.convert.CustomFormatToISO();
DEFINE ISOToUnix org.apache.pig.piggybank.evaluation.datetime.convert.ISOToUnix();

idb = LOAD 's3n://infectiousdata/idb/raw/pixel/ym=201310/d=25' USING
    com.twitter.elephantbird.pig.load.JsonLoader() AS ( json: map[] );
    idb = LOAD 's3n://infectiousdata/idb/raw/pixel/ym=201310/d=25' USING
    com.twitter.elephantbird.pig.load.JsonLoader() AS ( json: map[] );

idb = FOREACH idb GENERATE
    (int)json# 'pixel_id' AS pixel_id,
    (long)json# 'timestamp' AS timestamp,
    (chararray)json# 'ud1' AS ss_uid,
    (chararray)json# 'apn_user_id' AS apn_uid,
    'Conversion' AS channel,
    'FR' AS country_code,
    3 AS event_type;



idb_252 = FILTER idb BY (pixel_id == 252  and apn_uid != '0');
idb_252 = distinct idb_252;

idb_202 = FILTER idb BY (pixel_id == 202  and apn_uid != '0');
  
 join_converters = JOIN idb_202 by apn_uid, idb_252  BY apn_uid;
 attribution_tracked_users = FOREACH join_converters GENERATE
  idb_252::ss_uid AS ss_uid,
  idb_202::timestamp AS timestamp,
  idb_202::channel AS channel,
  idb_252::apn_uid AS apn_uid,
  idb_202::country AS country,
  idb_202::event_type AS event_type;
