REGISTER s3n://infectiousml/jars/elephant-bird-core-4.1.jar
REGISTER s3n://infectiousml/jars/elephant-bird-hadoop-compat-4.1.jar
REGISTER s3n://infectiousml/jars/elephant-bird-pig-4.1.jar
REGISTER s3n://infectiousml/jars/json-simple-1.1.jar
REGISTER s3n://infectiousml/jars/piggybank.jar;
REGISTER /home/hadoop/pig/udf/attr.py using streaming_python AS cumsum;

DEFINE CustomFormatToISO org.apache.pig.piggybank.evaluation.datetime.convert.CustomFormatToISO();
DEFINE ISOToUnix org.apache.pig.piggybank.evaluation.datetime.convert.ISOToUnix();
-------------------------------------------------------------------
-- load appnexus data
-------------------------------------------------------------------

apn = LOAD 's3n://infectiousdata/apn/raw/standard/ym=201310,d=25' using PigStorage ('\t') as  (auction_id_64:long, timestamp:chararray, user_tz_offset:int, width:int, height:int, media_type:int,
        fold_position:int, event_type:chararray, imp_type:int, payment_type:int, media_cost_dollars_cpm:float,revenue_type:int, buyer_spend:float, buyer_bid:float, ecp:float, eap:float, is_imp:int, is_learn:int,
        prediction_type_rev:int, other_user_id_64:chararray, ip_address:chararray, ip_address_trunc:chararray,geo_country:chararray, geo_region:chararray, operating_system:int, browser:int, language:int, venue_id:int,
        seller_member_id:int, publisher_id:int, site_id:int, site_domain:chararray, tag_id:int,external_inv_id:int, reserve_price:float, seller_revenue_cpm:float, media_buy_rev_share_pct:float, pub_rule_id:int,
        seller_currency:chararray, publisher_currency:chararray, publisher_exchange_rate:float,serving_fees_cpm:float, serving_fees_revshare:float, buyer_member_id:int, advertiser_id:int, brand_id:int, advertiser_frequency:int,
        advertiser_recency:int, insertion_order_id:int, campain_group_id:int,campaign_id:chararray, creative_id:int, creative_freq:int, creative_rec:int, cadence_modifier:float, can_convert:int, user_group_id:int, is_control:int,
        control_pct:int, control_creative_id:int, is_click:int,pixel_id:int, is_remarketing:int, post_click_conv:int, post_view_conv:int, post_click_revenue:float, post_view_revenue:float, order_id:chararray, external_data:chararray,
        pricing_type:chararray, booked_revenue_dollars:float,booked_revenue_adv_curr:float, commission_cpm:float, commission_revshare:float, auction_service_deduction:float, auction_service_fees:float, creative_overage_fees:float,
        clear_fees:float, buyer_currency:chararray,advertiser_currency:chararray, advertiser_exchange_rate:float, latitude:chararray, longitude:chararray, device_unique_id:chararray);

apn_1 = FILTER apn BY (event_type == 'imp' and  other_user_id_64 != '0' and campaign_id == '815');
apn_2 = FOREACH apn_1 GENERATE 
        ISOToUnix(CustomFormatToISO(timestamp, 'yyyy-MM-dd HH:mm:ss'))/1000 AS apn_timestamp,
        other_user_id_64 AS apn_uid,
        geo_country AS country,
        1 as event_type,
        'IM_impression' AS channel;

--apn_11 = FILTER apn BY (event_type == 'imp' and  other_user_id_64 != '0' and advertiser_id == '?');
--apn_22 = FOREACH apn_1 GENERATE 
        ISOToUnix(CustomFormatToISO(timestamp, 'yyyy-MM-dd HH:mm:ss'))/1000 AS apn_timestamp,
        other_user_id_64 AS apn_uid,
        geo_country AS country,
        1 as event_type,
        'Kronenbourg' AS channel;

 --apn_union = UNION apn_2, apn_22;       
-------------------------------------------------------------------
--load idb pixel data
-------------------------------------------------------------------

idb = LOAD 's3n://infectiousdata/idb/raw/pixel/ym=201310/d=25' USING
    com.twitter.elephantbird.pig.load.JsonLoader() AS ( json: map[] );
    idb = LOAD 's3n://infectiousdata/idb/raw/pixel/ym=201310/d=25' USING
    com.twitter.elephantbird.pig.load.JsonLoader() AS ( json: map[] );

idb = FOREACH idb GENERATE
    (long)json#'timestamp' AS timestamp,
    (chararray)json#'ud1' AS ss_uid,
    (chararray)json#'apn_user_id' AS apn_uid,
    (int)json#'pixel_id' AS pixel_id;

idb_1 = FILTER idb BY (pixel_id == 252  and apn_uid != '0');
idb_1 = distinct idb_1;

join_impressions = JOIN apn_2 by apn_uid, idb_1 BY apn_uid;

add the impressions = FOREACH join_impressions GENERATE
  idb_1 ::ss_uid AS ss_uid,
  apn_2::timestamp AS timestamp,
  apn_2::channel AS channel,
  apn_2::apn_uid AS apn_uid,
  apn_2::country AS country,
  apn_2::event_type AS event_type;



